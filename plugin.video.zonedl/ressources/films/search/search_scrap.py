﻿import os
import sys
import xbmcgui
import xbmcplugin
import urllib
import urllib2
from urllib2 import Request, urlopen
import urlparse
import subprocess
import linecache
import time
import cookielib
import re
import base64
from base64 import urlsafe_b64encode

# os.chdir("/storage/.kodi/addons/plugin.video.zonedl/ressources/films/search/data")
# os.chdir('data')
# print sys.path
print os.getcwd()
# print sys.argv[1]
addon_handle = int(sys.argv[1])
# xbmcplugin.setContent(addon_handle, 'movies')

# print os.getcwd()
# print os.listdir('/storage/.kodi/addons/plugin.video.zonedl/ressources/films/search/data')

# name_to_search = sys.argv[1]
# print sys.argv[1]



for fich in os.listdir('/storage/.kodi/addons/plugin.video.zonedl/ressources/films/search/data'):
	fichier = open(fich, "r")		
	title=fichier.readline().rstrip('\n\r')
	url_zonedl=fichier.readline().rstrip('\n\r')
	icon=fichier.readline().rstrip('\n\r')
	note=fichier.readline().rstrip('\n\r')
	quality=fichier.readline().rstrip('\n\r')
	duration=fichier.readline().rstrip('\n\r')
	genre=fichier.readline().rstrip('\n\r')
	director=fichier.readline().rstrip('\n\r')
	year=fichier.readline().rstrip('\n\r')
	synopsis=fichier.readline().rstrip('\n\r')
	
	handle_click='plugin://plugin.video.zonedlclick'
	url= handle_click + '?' + url_zonedl
	
	li = xbmcgui.ListItem(title, iconImage=icon, thumbnailImage=icon)
	li.setInfo('video', { 'year': year })
	li.setInfo('video', { 'genre': genre })
	li.setInfo('video', { 'duration': duration }) 
	li.setInfo('video', { 'rating': note })
	li.setInfo('video', { 'plot': synopsis })
	li.setInfo('video', { 'director': director })
	li.setLabel(title)
	# li.addContextMenuItems([("Rafraîchir",'Container.Refresh'),("Rechercher",'RunScript(/storage/.kodi/addons/plugin.video.zonedl/ressources/films/search/search_scrap.py, test1)')], True)
	# li.setProperty('IsPlayable', 'false')
	xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)
	# li.addContextMenuItems([('Informations','ActivateWindow(12003)')], True)

fichier.close()
xbmcplugin.endOfDirectory(addon_handle)


