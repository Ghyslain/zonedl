﻿#!/bin/sh

# Some changes for kodi use
#cd /volume1/zonedl/scrap_zonedl
cd /storage/.kodi/addons/plugin.video.zonedl/ressources

clear

###### Test suppression des anciens de la veille ######
#ssh root@192.168.1.72 rm /storage/.kodi/addons/plugin.video.zonedl/ressources/films/exclus/*.dat
#######################################################


old_IFS=$IFS  				
# nouveau séparateur de champ, le caractère fin de ligne      
IFS=$'\n' 

# echo date

date -I'seconds' | sed -e 's/T/\ /g' | sed -e 's/+.*$//g'
echo -e "\n"

rm films/exclus/*.dat
rm temp/*.html
rm temp/*.txt


curl -s 'http://www.zone-telechargement.com/exclus.html?tab=all&orderby_by=date&orderby_order=desc&displaychangeto=large' > temp/exclus.html
# curl -s http://www.zone-telechargement.com/exclus.html > temp/exclus.html

# Ne pas oublier de DL exclus.html en premier !
page_num_max=`cat temp/exclus.html | sed -n '/<div\ class=\"navigation2\">/,/<\/div>/p' | sed -e 's/^.*\.\.\.//g' | grep -o '[0-9]*[0-9]*[0-9]</a>' | sed -e 's/<\/a>//g' | sort -nr | sed -n '1p'`

for num_page in `seq 1 $page_num_max`
do
	page="http://www.zone-telechargement.com/exclus.html?page=$num_page&displaychangeto=large"
	name=`echo temp/exclus_$num_page.html`
	curl -s $page > $name
	echo ---------- Page $num_page downloading ----------
	name_title=`echo temp/title_$num_page.txt`

	# Obtenir les titres de la page voulue
	# cat $name | sed -n '/<div\ class=\"titrearticles\">/,/<\/div>/p' | grep '<a href=".*$' | sed -e 's/^.*\.html\">//g' | sed -e 's/<.*$//g'  > $name_title
	## Autre facon pour les titres - POUR LE MOMENT...
	cat $name | sed -n '/<div\ class=\"titrearticles\">/,/<\/div>/p' | grep -o '<a href=".*$' | grep -o '>.*<' | sed -e 's/^>//g' | sed -e 's/<.*$//g' > $name_title
	
	for title in `cat $name_title`
	do

		id=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/<\/div>/p' | grep $title | grep -o '/[0-9]*-' | sed -e 's/\///g'| sed -e 's/-//g'`
		title_dat=`echo $title.dat | tr "[A-Z]" "[a-z]" | tr " " "_" | sed -e 's/é/e/g' | sed -e 's/è/e/g' | sed -e 's/\.//g' | sed -e 's/://g' | sed -e 's/-//g' | sed -e "s/'/_/g" | sed -e 's/dat/\.dat/g' | sed -e 's/\*//g'`
		title_to_kodi=`echo $title | sed -e 's/*and\*/\&/g' | sed -e 's/-and-/\&/g' | sed -e "s/_/'/g"`		
		title_temp=`echo $title | sed -e 's/"/\&amp;#039;/g' | sed -e "s/'/\&#039;/g" | sed -e 's/_and_/_\&amp;_/g' | sed -e "s/_/\&amp;amp;#039;/g" | sed -e 's/*and*/\&amp;/g' | sed -e 's/-and-/\&amp;amp;amp;/g'`
		
		echo $title_to_kodi > films/exclus/$title_dat
		###Avec ça on obtient dans l'ordre: link (de la page zonedl) et link img | À retravailler pour avoir chaque lien dans une variable
		link_zonedl=`cat $name | sed -n '/<div\ class=\"titrearticles\">/,/<\/div>/p' | grep $title | grep -o 'http://www.zone-telechargement.com/films.*\.html'`
		icon_link=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | sed -e 's/.png/.png\n/g' | sed -e 's/.jpg/.jpg\n/g' | grep -Eo "http://images.zone-telechargement.com/360/http[s]*://.*.(png|jpg)"`
		echo $link_zonedl >> films/exclus/$title_dat
		echo $icon_link >> films/exclus/$title_dat

		note_bool=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep 'Note' | wc -l`
		if [ "$note_bool" = "0" ] ;
		then 
			note_bool="0"
			echo -e `` >> films/exclus/$title_dat
		else
			###Pour avoir la note, mais /5 juste ici
			note_sur_5=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o 'Note spectateurs.*)<br />' | grep -o '[0-9]*[0-9]*,*[0-9]/5' | sed -e 's/\/5//g' | sed -e 's/,/\./g'`
			###On a bien la note sur 10 ici
			note_sur_10=`awk -vp=$note_sur_5 'BEGIN{printf "%.1f\n" ,p * 2}'`
			echo $note_sur_10 >> films/exclus/$title_dat
		fi
		
		###Qualité, à voir ce que l'on en fait après
		quality=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o '<strong>.*</strong>' | sed -e 's/<strong>//g' | sed -e 's/<\/strong>//g'`
		echo $quality >> films/exclus/$title_dat

		###Durée en heure ici
		duration_hours=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o '[0-9]h\ *[0-9][0-9]*min' | sed -e 's/min//g' | sed -e 's/h/\./g' | sed -e 's/\ //g'`
		# duration_seconds=`echo "$duration_hours*3600/60" | bc -l`
		duration_seconds=`awk -vp=$duration_hours 'BEGIN{printf "%.1f\n" ,p * 3600}'`
		echo $duration_seconds >> films/exclus/$title_dat

		###Ici on a le(s) genre(s)
		genre=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o '<b>Genre\ :\ <\/b>.*<br\ \/><b>Durée' | sed -e 's/<b>Genre\ :\ <\/b>//g' | sed -e 's/<br\ \/><b>Durée//g'`
		echo $genre >> films/exclus/$title_dat

		###Réalisateur 
		director=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o '<b>Réalisateur : </b>.*<br /><b>Acteurs' | sed -e 's/<b>Réalisateur\ :\ <\/b>//g' | sed -e 's/<br\ \/><b>Acteurs//g'`
		echo $director >> films/exclus/$title_dat

		###Année de sortie
		bool_year=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o '<b>Date\ de\ sortie\ :\ </b>.*<br\ \/><b>Année\ de\ production' | grep -o '[0-9][0-9][0-9][0-9]' | wc -l`
		if [ "$bool_year" = "0" ] ;
			then 
			bool_year=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o 'Année de production.*[0-9][0-9][0-9][0-9]<' | grep -o '[0-9][0-9][0-9][0-9]' | wc -l`
			if  [ "$bool_year" = "0" ] ;
				then 
					year=`echo 0`
				else
					year=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o 'Année de production.*[0-9][0-9][0-9][0-9]<' | grep -o '[0-9][0-9][0-9][0-9]'`
			fi
		else
		year=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | grep -o '<b>Date\ de\ sortie\ :\ </b>.*<br\ \/><b>Année\ de\ production' | grep -o '[0-9][0-9][0-9][0-9]'`
		fi	
		echo $year >> films/exclus/$title_dat
		
		###Synopsis
		synopsis=`cat $name | sed -n '/<div\ class=\"maincont\">/,/<a\ style=\"display:none;\"\ href=\"http:\/\/www.zone-telechargement.com\/thelink.php\"/p' | sed -n '/<div\ id=\"news-id-/,/<div\ class=\"comdl\">/p' | sed -e 's/<div\ class=\"comdl\">//g' | grep $id | sed -e 's/^.*<div\ align=\"center\">//g' | sed -e 's/<br\ \/><\/div><\/div>//g' | sed -e "s/&#039;/'/g"`
		echo $synopsis >> films/exclus/$title_dat
		
		### Bande annonce ###
		youtube_api_key='AIzaSyBPaDSejXoL7-sNrBMfYTte1ept1HVcNY0'
		title_url_encoded=`echo $title | sed -e 's/,//g' | sed -e 's/://g' | sed -e 's/\.//g' | sed -e 's/\ /+/g'`
		youtube_id_video=`curl -s "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=$title_url_encoded+bande+annonce+vf&type=video&key={$youtube_api_key}" | grep videoId | sed -e 's/\"videoId\":\ \"\(.*\)\"/\1/g' | sed -e 's/\ //g'`
		echo $youtube_id_video >> films/exclus/$title_dat
		#################

		
		if [ "$note_bool" = "0" ] ;
		then
			echo "DONE - $title_to_kodi *** NO NOTE GIVEN"
		else
			echo DONE - $title_to_kodi
		fi
		# echo -e "\n"

	done
done

IFS=$old_IFS     

#scp films/exclus/*.dat root@192.168.1.72:/storage/.kodi/addons/plugin.video.zonedl/ressources/films/exclus/.

rm temp/*
