#!/bin/sh

cd /storage/.kodi/addons/plugin.video.zonedlclick

api_key='86f3733c206bd56bb1d2cb3177eff542'
start_b64='data:image/gif;base64,'
b64_captcha_raw=`openssl base64 -in captcha.gif` > /dev/null 2>&1
b64_captcha=`echo $start_b64$b64_captcha_raw | sed -e 's/\ //g'`

# upload du captcha a 2captcha
# response_upload=`curl -sX POST "http://2captcha.com/in.php?key=$api_key&method=base64" --data "body=$b64_captcha"`
response_upload=`curl -s -F "file=@/storage/.kodi/addons/plugin.video.zonedlclick/captcha.gif" "http://2captcha.com/in.php?key=$api_key&method=post"`

#### Captcha upload #####
if [ `echo $response_upload | grep 'OK|' | wc -l` = "1" ]
then
	captcha_id=`echo $response_upload | sed -e 's/OK|//g'`
	echo captcha id: $captcha_id
fi


if [ `echo $response_upload | grep 'ERROR' | wc -l` = "1" ]
then
	echo error:
	echo '1' > bool_captcha.txt
	if [ `echo $response_upload | grep 'ERROR_ZERO_BALANCE' | wc -l` = "1" ]
	then
		echo no money on account
	fi
fi
#########################


#### Get response #######
response_solved_captcha=`curl -s "http://2captcha.com/res.php?key=$api_key&action=get&id=$captcha_id"`

while [ `echo $response_solved_captcha | grep 'CAPCHA_NOT_READY' | wc -l` = "1" ]
do 
	sleep 1
	response_solved_captcha=`curl -s "http://2captcha.com/res.php?key=$api_key&action=get&id=$captcha_id"`
done

if [ `echo $response_solved_captcha | grep 'ERROR' | wc -l` = "1" ]
then
	echo error in decoding
	echo '1' > bool_captcha.txt
fi

if [ `echo $response_solved_captcha | grep 'OK|' | wc -l` = "1" ]
then
	> secure.txt
	captcha_text=`echo $response_solved_captcha | sed -e 's/OK|//g'`
	echo $captcha_text
	echo '0' > bool_captcha.txt
	echo $captcha_text > secure.txt
fi
#########################

