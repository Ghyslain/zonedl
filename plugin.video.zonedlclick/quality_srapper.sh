﻿#!/bin/sh
cd /storage/.kodi/addons/plugin.video.zonedlclick

# curl -L http://www.zone-telechargement.com/homep.html 

curl -L $1 > temp.html
# --cookie /storage/.kodi/addons/plugin.video.zonedlsearch/ressources/films/temp/cookie.txt
title=`cat temp.html | sed -n '/<div class=\"titrearticles\">/,/<div class=\"corps\">/p' | grep -o '<h1 style=.*</h1>' | grep -o '\">.*</h1>' | sed -e 's/\">//g' | sed -e 's/<\/h1>//g' | sed -e 's/<span\ itemprop=\"name//g' | sed -e 's/<\/span>//g'`
echo $title | sed -e 's/,//g' | sed -e 's/://g' | sed -e 's/\"//g' | sed -e 's/<//g' | sed -e 's/>//g' | sed -e 's/\*//g' > titre.txt

cat temp.html | grep 'Qualité[s]* également disponible[s]*' | sed -e 's/href/\nhref/g' | sed -e 's/<a\ title/\n\ntitre/g' | sed -e 's/<button\ class.*$//g' | grep '^titre\|^href' | sed -e 's/titre=\"//g' | sed -e 's/href=\"//g' | sed -e 's/\"//g' | sed -e 's/>//g' | sed -e "s/Téléchargez\ $title\ en\ //g" > quality.txt

# Partie pour rajouter la qualité de la page sur laquelle le script regarde #
# cat temp.html | grep 'font-size:12pt' | tr "<.*>" "\n" | grep Qualité | sed -e 's/Qualité\ //g' >> quality.txt

quality_current=`cat temp.html | grep 'font-size:12pt' | sed -e 's/\.//g' | tr "<.*>" "\n" | grep -E '(Qualité|[Bb][Ll][Uu][Rr][Aa][Yy]|\|)' | wc -l`

if [ "$quality_current" = "0" ]
then 
	echo No quality found >> quality.txt
else
	cat temp.html | grep 'font-size:12pt' | sed -e 's/\.//g' | tr "<.*>" "\n" | grep -E '(Qualité|[Bb][Ll][Uu][Rr][Aa][Yy]|\|)' >> quality.txt	
fi

echo $1 >> quality.txt
#############################################################################