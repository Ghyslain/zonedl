﻿import os
import sys
import xbmcgui
import xbmcplugin
import urllib
import urllib2
from urllib2 import Request, urlopen
import urlparse
import subprocess
import linecache
import time
import cookielib
import re
import base64
from base64 import urlsafe_b64encode

########## Changement du répertoire actuel ################
os.chdir("/storage/.kodi/addons/plugin.video.zonedlclick")
###########################################################

########## Check d'un téléchargement en cours ######################
bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','r')
bool_zonedl_running = bool_zonedl_running_file.read(1)
bool_zonedl_running_file.close()

# print bool_zonedl_running

if bool_zonedl_running == '1':
	dialog = xbmcgui.Dialog()
	ok = dialog.ok('Téléchargement en cours', "Un téléchargement est actuellement en cours, veuillez atendre la fin de celui-ci avant d'en lancer un autre...")
	# print 'le programme se stoppe'
	sys.exit(0)
#####################################################################


bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
bool_zonedl_running_file.write('1')
bool_zonedl_running_file.close()
# print 'le programme se lance'

## Création progress bar pour recherche des qualités #################
progress = xbmcgui.DialogProgress()
progress.create('Recherche des qualités disponibles', 'En cours...')
######################################################################


# Gestion de l'annulation de la recherche de qualité - au cas ou c'est trop long - arrêt du programme si "annulation" est cliqué #
if progress.iscanceled() == 1:
	# print 'le programme se stoppe'
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)
##################################################################################################################################

### Maj de la progress bar - récupération de l'url où on cherche les qualités en argument et de la date du film (pour bon scrap par kodi) #################
base_url = sys.argv[0]
addon_handle = int(sys.argv[1])
dialog = xbmcgui.Dialog()
url_zone_dl_raw = sys.argv[2]
print url_zone_dl_raw

i=1
for x in url_zone_dl_raw.split("?"):
	if i == 2:
		url_zone_dl = x;
	if i == 3:
		year = x;
	i+=1


file_year = open('/storage/.kodi/addons/plugin.video.zonedlclick/year.txt','w')
file_year.write(year)
file_year.close()

progress.update( 20, "En cours...")
###########################################################################################################

##### Lancement du scrapper bash qui recherche les qualités disponibles #########################
line_to_bash='/storage/.kodi/addons/plugin.video.zonedlclick/quality_srapper.sh ' + url_zone_dl
os.system(line_to_bash)
#################################################################################################


##### Check si zone téléchargement fonctionne bien ######
line_to_bash='/storage/.kodi/addons/plugin.video.zonedlclick/zonedl_check.sh'
os.system(line_to_bash)
http_code_zonedl_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/http_code_zonedl.txt','r')
http_code_zonedl = http_code_zonedl_file.readline()
# print http_code_zonedl
# progress.close()
# sys.exit(0)

# a=urllib.urlopen('http://www.zone-telechargement.com/homep.html')
# http_code_zonedl = a.getcode()
if http_code_zonedl != '200':
	dialog = xbmcgui.Dialog()
	progress.close()
	ok = dialog.ok('Problème avec le site Zone Téléchargement', 'Le site Zone Téléchargement ne fonctionne pas... Veuillez patienter' )
	# print 'le programme se stoppe'
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)

#########################################################


progress.update( 70, "En cours...")

####### Création de la liste des qualités disponibles pour le film choisi #########
fd = open('/storage/.kodi/addons/plugin.video.zonedlclick/quality.txt', 'r')
nbe_lines = 0
while fd.readline():
    nbe_lines += 1

liste_url = []
liste_name = []
i = 0
line_impair = 1 
line_pair = 0
while i < nbe_lines+1:

	line_to_list = linecache.getline('/storage/.kodi/addons/plugin.video.zonedlclick/quality.txt', i)
	
	if i == line_pair:
		liste_url.append(line_to_list)  
		line_pair += 2

	if i == line_impair:
		liste_name.append(line_to_list)  
		line_impair += 2
		
	i += 1

fd.close()
progress.update( 90, "En cours...")
progress.close()
#####################################################################################


######## Sélection de la qualité - arrêt du programme si la touche retour est appuyée (-1) ######
dialog = xbmcgui.Dialog()
title_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/titre.txt','r')
title = title_file.readline().rstrip('\n\r')
title_file.close()
disp_to_quality = 'Choisissez la qualité pour ' + title
quality_select = dialog.select(disp_to_quality, liste_name)

if quality_select == -1:
	# print quality_select
	# print 'le programme se stoppe'
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)

url_to_dl = liste_url[quality_select+1]
##################################################################################################


######################### Sélection de l'hébergeur voulu, fin du programme si la touche retour est cliquée (-1) #######
list_heberg = ['UPLOADED', 'UPTOBOX']
dialog = xbmcgui.Dialog()
heberg_bool = dialog.select("Quel hébergeur souhaitez-vous utiliser ?", list_heberg)

if heberg_bool == -1:
	# print 'le programme se stoppe'
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)
########################################################################################################################

### Ancienne façon de sélectionner l'hébergeur, pas bien car impossibilité d'annuler ! ###################
# heberg_bool = dialog.yesno("Selection de l'hébergeur","Veuillez sélectionner l'hébergeur que vous voulez utiliser", yeslabel='UPTOBOX', nolabel='UPLOADED')
# 0 -> UPLOADED et 1 -> UPTOBOX
# print heberg_bool
# sys.exit(0)
##########################################################################################################

###Création de la tache en background pour savoir où le script en est ###
progressBG = xbmcgui.DialogProgressBG()
progressBG.create('Récupération des liens DL protect','En cours...')
#########################################################################


progressBG.update(20,heading='Recherche de liens', message='En cours...')
###### Lancement du scrapper bash pour récupérer les liens DL protect sur zonedl, autant les premiums que les non premiums #########
line_to_bash2 = '/storage/.kodi/addons/plugin.video.zonedlclick/recup_dl_protect.sh ' + str(heberg_bool) + ' ' + url_to_dl
os.system(line_to_bash2)
####################################################################################################################################

##### Update de la progressbarBG ########################################
progressBG.update(40,heading='Recherche de liens', message='En cours...')
#########################################################################


# Ici je vide le fichier qui dit qu'il y a des liens invalides, a faire a chaque execution, car le script 
# bash qui verifiera rajoutera les valeurs a la suite, pour pouvoir gérer lorsqu'il y a plusieurs lien dl-protect
file_nb_invalid_links = open('/storage/.kodi/addons/plugin.video.zonedlclick/nbr_links_invalid.txt','w')
file_nb_invalid_links.close()
file_nb_invalid_links = open('/storage/.kodi/addons/plugin.video.zonedlclick/not_checked_links.txt','w')
file_nb_invalid_links.close()


############################### Si aucun lien premium n'a été trouvé, on essaie avec les non premiums | ATTENTION, si premiums trouvés MAIS invalides, ne regarde pas les non premiums ##########
file_dl_protect_premium = open('/storage/.kodi/addons/plugin.video.zonedlclick/heberg_premium.txt', 'r')
nbe_lines_premium = 0
while file_dl_protect_premium.readline():
    nbe_lines_premium += 1
file_dl_protect_premium.close()

file_dl_protect = open('/storage/.kodi/addons/plugin.video.zonedlclick/heberg.txt', 'r')
nbe_lines = 0
while file_dl_protect.readline():
    nbe_lines += 1
file_dl_protect.close()

print nbe_lines
print nbe_lines_premium

bool_heberg_premium = 1
bool_heberg = 1

if nbe_lines_premium == 0:
	bool_heberg_premium = 0
	if nbe_lines == 0:
		bool_heberg = 0
		dialog = xbmcgui.Dialog()
		ok = dialog.ok('Aucun liens trouvés', 'Essayez un autre hébergeur ou une autre qualité.' )
		progressBG.update(100,heading='Aucun lien trouvés', message='...')
		progressBG.close()
		# print 'le programme se stoppe'
		bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
		bool_zonedl_running_file.write('0')
		bool_zonedl_running_file.close()
		sys.exit(0)
################################################################################################################################

###### Check si Unprotector fonctionne ######
# progressBG.update(50,heading='Bypass DL protect', message='Vérification du service Unprotector...')
# a=urllib.urlopen('http://unprotector.jimrobs.com/')
# http_code_unprotector = a.getcode()
# if http_code_unprotector == 503:
	# dialog = xbmcgui.Dialog()
	# ok = dialog.ok('Problème avec Unprotector de JimRobs', 'Le service Unprotector de JimRobs ne fonctionne pas...' )
	# progressBG.close()
	# bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	# bool_zonedl_running_file.write('0')
	# bool_zonedl_running_file.close()
	# sys.exit(0)
#############################################

# #### Gestion du captcha, à garder, mais doit être remplacée par l'utilisation de unprotector (à garder pour erreur des fois) ###########
# file_dl_protect_premium = open('heberg_premium.txt', 'r')
# for line in file_dl_protect_premium.readlines():
	# # line est l'url dl-protect, si il y en a plusieurs, elle changera a chaque execution de la boucle
	# url_dl_protect = line
	
	# # On lance le script bash qui clique sur continuer sur dl protect, si il y a un captcha a rentrer il écrit 1 dans bool_captcha.txt
	# line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/script_bash_click_continue.sh ' + url_dl_protect
	# os.system(line_to_bash)

	# # On lit la valeur du booléen captcha, pour savoir si on doit rentrer un captcha ou non
	# file_bool_captcha = open('bool_captcha.txt', 'r')
	# bool_captcha = file_bool_captcha.readline().rstrip('\n\r')
	# file_bool_captcha.close()

	# progressBG.update(60,heading='Bypass DL protect', message='En cours...')

	# to_display_while_entering_captcha = 'Entrer le captcha pour lancer le téléchargement...'

	# if bool_captcha == '1':
		# progressBG.update(70,heading='Bypass DL protect', message='Veuillez saisir le captcha')
		# while bool_captcha != '0':
			
			# dialog_captcha = xbmcgui.Dialog()
			# captcha_code = dialog_captcha.input(to_display_while_entering_captcha, type=xbmcgui.INPUT_ALPHANUM)
			# progressBG.update(80,heading='Bypass DL protect', message='Vérification du captcha...')
			# file_secure = open('secure.txt','w')
			# file_secure.write(captcha_code)
			# file_secure.close()
			# to_display_while_entering_captcha = 'MAUVAIS CAPTCHA, REESSAYER - PATIENTEZ UN PEU AVANT DE SOUMETTRE (00000 Pour stopper)'	
			# line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/captcha_sender.sh'
			# os.system(line_to_bash)
			# file_bool_captcha = open('bool_captcha.txt', 'r')
			# bool_captcha = file_bool_captcha.readline().rstrip('\n\r')
			# file_bool_captcha.close()
			# # # Obligé d'attendre 5 sec pour que le nouveau captcha s'affiche à l'écran... Du moins de la façon dont je l'affiche... 
			# time.sleep(5)		
			# if captcha_code == '00000':
				# break			
		
# file_dl_protect_premium.close()
###########################################################################################################################################		

progressBG.update(50,heading='Bypass DL protect', message='En cours...')


###### Tentative gestion captcha personnelle ###########
if bool_heberg_premium == 1:
	file_dl_protect_premium = open('heberg_premium.txt', 'r')
if bool_heberg_premium == 0:
	file_dl_protect_premium = open('heberg.txt', 'r')

for line in file_dl_protect_premium.readlines():
	# line est l'url dl-protect, si il y en a plusieurs, elle changera a chaque execution de la boucle
	url_dl_protect = line
	
	# On lance le script bash qui clique sur continuer sur dl protect, si il y a un captcha a rentrer il écrit 1 dans bool_captcha.txt + dl l'image du captcha
	line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/script_bash_click_continue.sh ' + url_dl_protect
	os.system(line_to_bash)

	# On lit la valeur du booléen captcha, pour savoir si on doit rentrer un captcha ou non
	file_bool_captcha = open('bool_captcha.txt', 'r')
	bool_captcha = file_bool_captcha.readline().rstrip('\n\r')
	file_bool_captcha.close()
	progressBG.update(60,heading='Bypass DL protect', message='En cours...')

	if bool_captcha == '1':
		while bool_captcha != '0':
			line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/2captcha_use.sh'
			os.system(line_to_bash)
			line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/captcha_sender.sh'
			os.system(line_to_bash)
			file_bool_captcha = open('bool_captcha.txt', 'r')
			bool_captcha = file_bool_captcha.readline().rstrip('\n\r')
			file_bool_captcha.close()		
file_dl_protect_premium.close()
########################################################		

##### Utilisation de unprotector by Jimrobs #####
# progressBG.update(70,heading='Bypass DL protect...', message='Unprotector by JimRobs')
# line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/unprotector_use.sh'
# os.system(line_to_bash)
#################################################

#### Essai de supprimer l'image du captcha ######
# captcha_file = open('captcha.gif','w')
# captcha_file.close()
#################################################


########################################## PARTIE NON TESTEE ENCORE ###################################
# Gestion de l'erreur si aucun lien de l'hébergeur n'a pu être trouvé - après avoir passé dl-protect... 
file_links = open('/storage/.kodi/addons/plugin.video.zonedlclick/not_checked_links.txt','r')
nbe_lines = 0
while file_links.readline():
    nbe_lines += 1
file_links.close()
if nbe_lines == 0:
	dialog = xbmcgui.Dialog()
	ok = dialog.ok('Aucun liens a été trouvé', 'Essayez un autre hébergeur...' )
	progressBG.update(100,heading='Aucun lien trouvés', message='...')
	progressBG.close()
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)
######################################################################################################

line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/links_validation.sh'
os.system(line_to_bash)
file_nbr_links_invalid = open('/storage/.kodi/addons/plugin.video.zonedlclick/nbr_links_invalid.txt','r')

bool_ok_to_dl = 1
for line in file_nbr_links_invalid.readline().rstrip('\n\r'):
	if line == '1':
		progressBG.update(90,heading='ATTENTION', message='Certains liens ne sont plus valides')
		dialog = xbmcgui.Dialog()
		ok = dialog.ok('Certains liens ne sont plus valides', 'Veuillez essayer avec un autre hébergeur' )
		progressBG.close()
		bool_ok_to_dl = 0
		bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
		bool_zonedl_running_file.write('0')
		bool_zonedl_running_file.close()
		sys.exit(0)

file_nbr_links_invalid.close()

if bool_ok_to_dl == 1:
	progressBG.update(90,heading='Tout est ok - Finalisation', message='Le téléchargement va débuter...')
	line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/download_task_launcher.sh'
	os.system(line_to_bash)

# progressBG.update(100,heading='Finalisation', message='Dernières tâches en cours...')	
# progressBG.close()

file_nb_invalid_links = open('/storage/.kodi/addons/plugin.video.zonedlclick/fich.log','w')
file_nb_invalid_links.close()

line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/deamon_dl.sh | tee -a /storage/.kodi/addons/plugin.video.zonedlclick/fich.log'
os.system(line_to_bash)

progressBG.update(100,heading='Finalisation', message='Dernières tâches en cours...')	
# time.sleep(20)
progressBG.close()

bool_dl_finished_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_dl_finished.txt','r')
bool_dl_finished = bool_dl_finished_file.read(1)
bool_dl_finished_file.close()

if bool_dl_finished == '0':
	bool_dl_finished_real = bool_dl_finished 
	progressBG.create(heading='Téléchargement en cours...', message=title)

if bool_dl_finished == '':
	bool_dl_finished_real = bool_dl_finished 
	bool_dl_finished = '0'
	progressBG.create(heading='Téléchargement en cours...', message=title)
	
while bool_dl_finished == '0':
	if bool_dl_finished_real == '':
		time.sleep(20)
		bool_dl_finished_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_dl_finished.txt','r')
		bool_dl_finished = bool_dl_finished_file.read(1)
		bool_dl_finished_file.close()
		if bool_dl_finished == '':
			bool_dl_finished_real = bool_dl_finished 
			bool_dl_finished = '0'
		if bool_dl_finished == '0':
			bool_dl_finished_real = bool_dl_finished 

	if bool_dl_finished_real == '0':
		line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/deamon_dl.sh | tee -a /storage/.kodi/addons/plugin.video.zonedlclick/fich.log'
		os.system(line_to_bash)
		time.sleep(30)
		bool_dl_finished_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_dl_finished.txt','r')
		bool_dl_finished = bool_dl_finished_file.read(1)
		bool_dl_finished_file.close()
		if bool_dl_finished == '':
			bool_dl_finished_real = bool_dl_finished 
			bool_dl_finished = '0'
		if bool_dl_finished == '0':
			bool_dl_finished_real = bool_dl_finished 

	percent_downloaded_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/percent_downloaded.txt','r')
	percent_downloaded = percent_downloaded_file.readline().rstrip('\n\r')
	percent_downloaded_file.close()
	progressBG.update(int(percent_downloaded),heading='Téléchargement en cours...', message=title)

progressBG.close()

if bool_dl_finished == '':
	dialog = xbmcgui.Dialog()
	ok = dialog.ok('PAS EU LE TEMPS !', '!!' )
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)



if bool_dl_finished == '1':
	dialog = xbmcgui.Dialog()
	dl_done_to_disp = title + " se trouve maintenant dans votre bibliothèque !"
	ok = dialog.ok('Téléchargement terminé !', dl_done_to_disp )
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)

if bool_dl_finished == '2':
	dialog = xbmcgui.Dialog()
	dl_error_to_disp = "Il y a eu un problème lors du téléchargement de " + title
	ok = dialog.ok('ERREUR', dl_error_to_disp)
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)
	
if bool_dl_finished == '3':
	details_error_dl_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/details_error_download.txt','r')
	details_error_dl = details_error_dl_file.readline().rstrip('\n\r')
	details_error_dl_file.close()
	dialog = xbmcgui.Dialog()
	ok = dialog.ok('ERREUR', details_error_dl)
	bool_zonedl_running_file = open('/storage/.kodi/addons/plugin.video.zonedlclick/bool_zonedl_running_file.txt','w')
	bool_zonedl_running_file.write('0')
	bool_zonedl_running_file.close()
	sys.exit(0)

### On se délogue de l'appi de DonwloadStation #####
# line_to_bash = '/storage/.kodi/addons/plugin.video.zonedlclick/unlog_nas.sh'
# os.system(line_to_bash)
####################################################

####### On rend la main à KODI ##########
xbmcplugin.endOfDirectory(addon_handle)
#########################################